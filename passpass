#!/usr/bin/env python3

""" Passpass is a tool using pass to manage set of credentials.

This tool is highly inspired from weboob backends file and is weboob compatible.
This will store set of credentials in a SECRETS_FILE with the format:
```
[name]
key = value
key1 = value1
...

[name2]
keyb = valub
keyb1 = valueb1
...

```

Python objects equivalence for SECRETS_FILE:
```
{               ------------------------------------+
    name : {        ---------------------+          |
        key: value,    ----+             |          |
        key1: value1,      } fields      } secret   |
        ...            ----+             |          |
    },              ---------------------+          |
    name2 : {                                       } secrets
        keyb: valueb,                               |
        keyb1: valueb1,                             |
        ...                                         |
    },                                              |
    ...                                             |
}               ------------------------------------+
```

"""

import argparse
import os
import re
import shutil
import subprocess
import sys
import tempfile
import uuid
from collections import OrderedDict


SECRET_WORDS = ('password', )
SECRETS_FILE = os.getenv('SECRETS_FILE') or os.path.expanduser('~/.password-store/.secrets')

errors = set()


# CIPHER
def encrypt_secret_value(key, value, name):
    """ Use pass to encrypt data """

    if re.match(r'\`pass show .*\`', value):
        return value
    elif key in SECRET_WORDS:
        cmd = ['pass', 'insert', '%s/%s' % (name, key)]

        proc = subprocess.Popen(
            cmd, stdin=subprocess.PIPE,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        stored_value = 2 * ('%s\n' % value)
        proc.communicate(stored_value.encode('utf-8'))

        if proc.returncode == 0:
            return '`pass show %s/%s`' % (name, key)
        else:
            errors.add(name)
            return value

def encrypt_secrets(secrets):
    """ Encrypt secrets

    Encrypt all fields with key in SECRET_WORDS for each secret.
    """

    for name, fields in secrets.items():
        for key, value in fields.items():
            if (
                key in SECRET_WORDS
                and not re.match(r'\`pass show .*\`', value)
            ):
                secrets[name][key] = encrypt_secret_value(key, value, name)

    return secrets

def decrypt_secret_value(command, name):
    """ Use pass to decrypt data """

    if not re.match(r'\`pass show .*\`', command):
        return command

    cmd = command.replace('`', '').split()

    proc = subprocess.Popen(
        cmd, stdin=subprocess.PIPE,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    out, err = proc.communicate()

    if proc.returncode == 0:
        out = out.decode('utf-8')
        return out.strip()
    else:
        errors.add(name)
        return command

def decrypt_secrets(secrets):
    """ Decrypt secrets

    Decrypt all fields with key in SECRET_WORDS for each secret.
    """

    for name, fields in secrets.items():
        for key, value in fields.items():
            if key in SECRET_WORDS:
                secrets[name][key] = decrypt_secret_value(value, name)

    return secrets


# UTILS
def parse_secrets():
    """ Convert secrets file elements into python objects """

    secrets = OrderedDict()
    with open(SECRETS_FILE) as f:
        current_name = ''
        seen = set()
        for line in f:
            line = line.strip()

            name = re.search(r'^\[(?P<name>.*)\]$', line)
            fields = re.search(r'(?P<key>[\w ]+)\=(?P<value>.*)', line)
            comment = re.search(r'^#.*', line)

            if comment:
                # TODO: do not erase comments which start with #
                # for the moment use, _comment to comment
                continue

            if name:
                current_name = name.group('name')
                if current_name in seen:
                    sys.exit('There are 2 secrets with same name, please fix it!')
                    return
                else:
                    secrets[current_name] = OrderedDict()
                    seen.add(current_name)
            elif fields and current_name:
                key, value = fields.groups()
                secrets[current_name][key.strip()] = value.strip()
            elif line and not current_name:
                print('File line is not parsed: %s and will be removed.' % line)
                continue
            elif line and not fields:
                errors.add(current_name)
    return secrets

def remove_secret_from_pass(command):
    """ Use pass to remove encrypted data """

    if not re.match(r'\`pass show .*\`', command):
        return False

    cmd = command.replace('`', '').replace('show', 'rm').split()

    print('Try to remove %s from pass.' % cmd[-1])
    proc = subprocess.Popen(
        cmd, stdin=subprocess.PIPE,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    out, err = proc.communicate()

    if proc.returncode == 0:
        print('Remove %s from pass success.' % cmd[-1])
        return True
    else:
        print('Remove %s from pass failed, maybe does not exists anymore.' % cmd[-1])
        errors.add(cmd[-1])
        return False

def remove_secrets_from_pass(secrets):
    """ Remove secrets

    Remove all pass elements corresponding with key in SECRET_WORDS for each secret.
    """

    for name, fields in secrets.items():
        for key, value in fields.items():
            if key in SECRET_WORDS:
                remove_secret_from_pass(value)

def write_secrets_into_file(secrets):
    """ Rewrite SECRETS_FILE with updated secrets """

    with tempfile.NamedTemporaryFile('w', delete=False, dir=os.path.dirname(SECRETS_FILE)) as outp:
        for name, fields in secrets.items():
            print('[%s]' % name, file=outp)
            for key, value in fields.items():
                print('%s = %s' % (key, value), file=outp)
            print('', file=outp)

    os.rename(outp.name, SECRETS_FILE)


# FORMATTER
def secrets_formatter(secrets):
    output_str = ''
    for name, fields in secrets.items():
        output_str = '%s\n[%s]' % (output_str, name)
        for key, value in fields.items():
            output_str = '%s\n%s = %s' % (output_str, key, value)
        output_str = '%s\n' % output_str
    return output_str


# CLI COMMANDS
def cmd_encrypt_all():
    print('Get all secrets and encrypt field in SECRET_WORDS ...')
    secrets = parse_secrets()
    secrets = encrypt_secrets(secrets)
    write_secrets_into_file(secrets)
    print('Done.')

def cmd_add_secret(new_secret):
    secrets = parse_secrets()

    for name, fields in new_secret.items():
        if name in secrets:
            print('This name %s is already used, please change secret name.' % name)
            return {}, []

        for key, value in fields.items():
            if key in SECRET_WORDS and value:
                print('**WARNING**  The new secret %s is probably in plain text in your command history.' % name)
                print('')

            if not value:
                new_secret[name][key] = input('Enter %s value: ' % key)

    new_secret = encrypt_secrets(new_secret)
    secrets.update(new_secret)
    write_secrets_into_file(secrets)

    print('New secret added:')
    print(secrets_formatter(new_secret))

def cmd_get_secrets(secrets_name=None, decrypt=False, all_secrets=False):
    secrets = parse_secrets()

    if all_secrets:
        asked_secrets = secrets
    elif secrets_name:
        # TODO: change into ordered dict
        asked_secrets = {
            name: value for name, value
            in secrets.items()
            if name in secrets_name
        }
    else:
        print('There are nothing to show, please use -a to show all secrets.')
        return

    if decrypt:
        asked_secrets = decrypt_secrets(asked_secrets)

    print(secrets_formatter(asked_secrets))

def cmd_remove_secrets(secrets_name=None, soft_delete=False, all_secrets=False):
    secrets = parse_secrets()

    if all_secrets:
        secrets_name = [name for name in secrets]
        secrets_to_delete = secrets.copy()
    elif secrets_name:
        secrets_to_delete = {
            name: fields for name, fields
            in secrets.items()
            if name in secrets_name
        }
    else:
        print('There are nothing to be removed, please use -a to remove all secrets.')
        return

    for name in secrets_name:
        if name in secrets:
            print('%s will be removed from SECRETS_FILE...' % name)
            del secrets[name]
        else:
            print('%s is not in SECRETS_FILE.' % name)

    if not soft_delete:
        remove_secrets_from_pass(secrets_to_delete)

    write_secrets_into_file(secrets)
    print('End of remove operation.')

def cmd_update_secret(secrets_to_update):
    secrets = parse_secrets()

    updated_secrets = {}
    for name, fields in secrets_to_update.items():
        if name not in secrets:
            print('%s is not in SECRETS_FILE, cannot be updated.' % name)
            continue

        for key, value in fields.items():
            if key in SECRET_WORDS and value:
                print('**WARNING**  The new secret %s is probably in plain text in your command history.' % name)
                print('')

            if not value:
                secrets[name][key] = input('Enter %s value: ' % key)
            else:
                secrets[name][key] = value
        updated_secrets[name] = secrets[name]

    updated_secrets = encrypt_secrets(updated_secrets)
    secrets.update(updated_secrets)
    write_secrets_into_file(secrets)


# MAIN
if __name__ == "__main__":
    if not shutil.which('pass'):
        print('The "pass" tool could not be found, please install it and init it.')
        sys.exit(os.EX_UNAVAILABLE)

    if not os.path.exists(SECRETS_FILE):
        print('Secrets file does not exist, please create the file.')
        sys.exit(os.EX_NOINPUT)

    parser = argparse.ArgumentParser(description='Use pass to encrypt password')

    # add some subparsers like 'get' or 'add'
    # in order to manipulate secrets without
    # needed to see the file
    subparsers = parser.add_subparsers(dest='command')

    # LIST: list secret(s) name(s) from file
    parser_list = subparsers.add_parser('list', help='List secrets name from SECRETS_FILE.')

    # GET: get secret(s) from file
    parser_get = subparsers.add_parser('get', help='Get secrets from SECRETS_FILE.')
    parser_get.add_argument(
        'names', type=str, nargs='*',
        help='Names of secrets.'
    )
    parser_get.add_argument(
        '-d', '--decrypt-secrets', action='store_true',
        help='Decrypt fields with pass.'
    )
    parser_get.add_argument(
        '-a', '--all', action='store_true',
        help='Get all secret in SECRETS_FILE. Warning: this option have priority.'
    )

    # REMOVE: remove secret(s) from file
    parser_remove = subparsers.add_parser('remove', help='Remove secrets from SECRETS_FILE.')
    parser_remove.add_argument(
        'names', type=str, nargs='*',
        help='Names of secrets.'
    )
    parser_remove.add_argument(
        '-s', '--soft-delete', action='store_true',
        help='Only delete on SECRETS_FILE and not in pass.'
    )
    parser_remove.add_argument(
        '-a', '--all', action='store_true',
        help='Remove all secret from SECRETS_FILE. Warning: this option have priority.'
    )

    # ADD: add secret in file
    parser_add = subparsers.add_parser('add', help='Add new secret in SECRETS_FILE.')
    parser_add.add_argument(
        'name', type=str,
        help='Name of the new secret.'
    )
    parser_add.add_argument(
        'args', type=str, nargs='*',
        help='All fields of the new secret, empty value for interactive input. \
        Format: key=value.'
    )

    # UPDATE: update field from secret
    parser_update = subparsers.add_parser(
        'update',
        help='Update existing secret. Warning: this will strictly replace fields.'
    )
    parser_update.add_argument(
        'name', type=str,
        help='Name of the secret to update'
    )
    parser_update.add_argument(
        'args', type=str, nargs='*',
        help='All fields to be updated, empty value for interactive input. Format: key=value.'
    )

    args = parser.parse_args()

    secrets = {}
    if args.command == 'get':
        cmd_get_secrets(
            secrets_name=args.names,
            decrypt=args.decrypt_secrets,
            all_secrets=args.all
        )

    elif args.command == 'list':
        secrets = parse_secrets()
        print('\n'.join(name for name in secrets))

    elif args.command == 'remove':
        cmd_remove_secrets(
            secrets_name=args.names,
            soft_delete=args.soft_delete,
            all_secrets=args.all
        )

    elif args.command == 'add':
        try:
            secrets[args.name.strip()] = {
                arg[0].strip(): arg[1].strip() for arg
                in (el.split('=', 1) for el in args.args)
            }
        except IndexError:
            print('Please respect the format:\nname key1=value1 key2=value2 ...')
            print('You can use empty value like "key=" to trigger the interactive mode')
        else:
            cmd_add_secret(secrets)

    elif args.command == 'update':
        try:
            secrets[args.name.strip()] = {
                arg[0].strip(): arg[1].strip() for arg
                in (el.split('=', 1) for el in args.args)
            }
        except IndexError:
            print('Please respect the format:\nname key1=value1 key2=value2 ...')
            print('You can use empty value like "key=" to trigger the interactive mode')
        else:
            cmd_update_secret(secrets)
    elif not args:
        cmd_encrypt_all()

    if errors:
        print('**ERROR**  Error(s) occured during process for secret(s):')
        print(' '.join(list(errors)))
